---
title: Langage Markdown
subtitle: Introduction à la syntaxe
date: 2022-02-20
tags: ["exemple", "markdown", "images"]
---

Vous pouvez écrire directement du [Markdown](https://fr.wikipedia.org/wiki/Markdown), et il sera automatiquement converti en une page web.

<!--more-->

N'hésitez pas à [prendre 5 minutes pour apprendre la syntaxe du langage Markdown](http://markdowntutorial.com/fr/) ; cela vous apprendra comment mettre du texte *en italiques* ou **en gras**, comment définir des titres de différents niveaux, comment construire des tables, et ainsi de suite.

**Ceci est du texte en gras**

## Voilà un titre secondaire

Que diriez vous d'une délicieuse crêpe ?

<!-- ceci est un commentaire, pour préciser que c'est trop bon les crêpes ! -->

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Et pourquoi pas une table pour manger autour ?

| Colonne | Autre colonne | Et une autre |
| :------ |:------------- | :----------- |
| Cinq    | Six           | Quatre       |
| Dix     |    ʕᵔᴥᵔʔ      | Neuf         |
| Sept    | Huit          | ◖(｡◕‿◕｡)◗ ♪♫ |
| Deux | Trois | Deux mille vingt deux |

