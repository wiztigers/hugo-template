---
title: Encore un autre
subtitle: Mais celui-ci a un sous-titre!
date: 2022-01-10
---

Remarquez les méta-données au début du fichier correspondant à chaque article.
Celui-ci en utilise trois :
* Une méta-donnée renseignant le titre de l'article : `title`.
  Remarquez que la valeur de la méta-donnée `title` et le nom du fichier lui-même peuvent n'avoir rien en commun.
* Une autre renseignant la date de l'article : `date`.
  Remarquez que vous pouvez indiquer n'importe quelle date, mais il est _impératif_ de respecter le format **AAAA-MM-DD**.
* Une troisième, qui permet d'ajouter un sous-titre à l'article : `subtitle`.

Les deux exemples précédents utilisaient déjà les deux premières méta-données, `title` et `date`.
